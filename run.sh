#!/bin/bash
python challenge1/spark/main.py --source=challenge1/dataset/nyc_airlines.csv --db_host=localhost --db_port=5432 --db_username=de_challenge --db_password=Password1234** --db_database=dw_flights --db_table=airlines
python challenge1/spark/main.py --source=challenge1/dataset/nyc_airports.csv --db_host=localhost --db_port=5432 --db_username=de_challenge --db_password=Password1234** --db_database=dw_flights --db_table=airports
python challenge1/spark/main.py --source=challenge1/dataset/nyc_planes.csv --db_host=localhost --db_port=5432 --db_username=de_challenge --db_password=Password1234** --db_database=dw_flights --db_table=planes
python challenge1/spark/main.py --source=challenge1/dataset/nyc_weather.csv --db_host=localhost --db_port=5432 --db_username=de_challenge --db_password=Password1234** --db_database=dw_flights --db_table=weather
python challenge1/spark/main.py --source=challenge1/dataset/nyc_flights.csv --db_host=localhost --db_port=5432 --db_username=de_challenge --db_password=Password1234** --db_database=dw_flights --db_table=flights

python challenge1/spark/main.py --db_host=localhost --db_port=5432 --db_username=de_challenge --db_password=Password1234** --db_database=dw_flights --destination=result
