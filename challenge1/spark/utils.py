from pyspark.sql import DataFrame, SparkSession

def read_csv(spark: SparkSession, path: str, schema=None) -> DataFrame:

    if not schema:
        return spark.read.options(header='True', inferSchema='True', delimiter=',', nullValues='NA').csv(path)
    else:
        return spark.read.options(header='True', delimiter=',', nullValues='NA').csv(path, schema=schema)
    
def write_csv(df: DataFrame, path: str):
    df.coalesce(1).write.options(header='True', delimiter=',').mode('overwrite').csv(path)

def read_from_db(spark: SparkSession, jdbc_url: str, table: str, user: str, password: str) -> DataFrame:
    return spark.read.format("jdbc").option("url", jdbc_url) \
        .option("driver", "org.postgresql.Driver") \
        .option("dbtable", table) \
        .option("user", user) \
        .option("password", password).load()

def write_to_db(df: DataFrame, jdbc_url: str, table: str, user: str, password: str):
    df.write.format("jdbc").option("url", jdbc_url) \
        .option("driver", "org.postgresql.Driver") \
        .option("dbtable", table) \
        .option("user", user) \
        .option("password", password).mode('append').save()
