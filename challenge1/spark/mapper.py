from pyspark.sql import DataFrame


def _build_map(mapped_columns: dict):
    expr = list()
    for key, value in mapped_columns.items():
        expr.append(f"CAST({key} AS {value['type']}) AS {value['name']}")

    return expr

def mapper(df: DataFrame, mapped_columns: dict) -> DataFrame:
    expr = _build_map(mapped_columns)

    return df.selectExpr(*expr)
