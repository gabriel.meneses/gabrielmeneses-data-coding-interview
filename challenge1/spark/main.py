import argparse
from pyspark.sql import SparkSession

from utils import *
from schema import schema, primary_keys
from mapper import *
from model import *

def main(argv=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('--db_host', dest='db_host', type=str)
    parser.add_argument('--db_port', dest='db_port', type=int)
    parser.add_argument('--db_username', dest='db_username', type=str)
    parser.add_argument('--db_password', dest='db_password', type=str)
    parser.add_argument('--db_database', dest='db_database', type=str)
    parser.add_argument('--db_table', dest='db_table', type=str)
    parser.add_argument('--source', dest='source', type=str)
    parser.add_argument('--destination', dest='destination', type=str, default=None)
    args = parser.parse_args(argv)

    spark = SparkSession.builder.config("spark.jars", "jars/postgresql-42.6.0.jar").appName("Code test").getOrCreate()
    jdbc_url = f"jdbc:postgresql://{args.db_host}:{args.db_port}/{args.db_database}"

    if args.destination is None: 
        df = read_csv(spark, args.source)

        df = mapper(df, mapped_columns=schema[args.db_table])

        df = df.dropDuplicates(primary_keys[args.db_table])

        write_to_db(df, jdbc_url=jdbc_url, table=args.db_table, user=args.db_username, password=args.db_password)

    else:
        load_database(spark, jdbc_url, user=args.db_username, password=args.db_password)
        df = run_query(spark)
        write_csv(df, path=args.destination)

if __name__ == "__main__":
    main()
