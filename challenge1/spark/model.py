from utils import read_from_db

query = """
SELECT airlines.name AS airline,
       CONCAT(flights.carrier, '-', flights.flight) AS flight,
       CONCAT(flights.year, '-', LPAD(flights.month, 2, '0'), '-', LPAD(flights.day, 2, '0')) AS flight_date,
       flights.tailnum AS tailnum,
       DATE_FORMAT(TO_TIMESTAMP(LPAD(CAST(flights.sched_dep_time AS STRING), 4, '0'), 'hhmm'), 'hh:mm') AS sched_dep_time,
       DATE_FORMAT(TO_TIMESTAMP(LPAD(CAST(flights.actual_dep_time AS STRING), 4, '0'), 'hhmm'), 'hh:mm') AS actual_dep_time,
       flights.dep_delay AS dep_delay,
       DATE_FORMAT(TO_TIMESTAMP(LPAD(CAST(flights.sched_arr_time AS STRING), 4, '0'), 'hhmm'), 'hh:mm') AS sched_arr_time,
       DATE_FORMAT(TO_TIMESTAMP(LPAD(CAST(flights.actual_arr_time AS STRING), 4, '0'), 'hhmm'), 'hh:mm') AS actual_arr_time,
       flights.arr_delay AS arr_delay,
       origin_airport.faa AS origin_faa,
       origin_airport.name AS origin_name,
       origin_airport.latitude AS origin_latitude,
       origin_airport.longitude AS origin_longitude,
       origin_airport.altitude AS origin_altitude,
       weather.temp AS origin_temp,
       weather.dewp AS origin_dewp,
       weather.humid AS origin_humid,
       weather.precip AS origin_precip,
       weather.pressure AS origin_pressure,
       weather.visib AS origin_visib,
       dest_airport.faa AS dest_faa,
       dest_airport.name AS dest_name,
       dest_airport.latitude AS dest_latitude,
       dest_airport.longitude AS dest_longitude,
       dest_airport.altitude AS dest_altitude,
       flights.air_time AS air_time,
       flights.distance AS distance,
       planes.model AS airplane_model,
       planes.seats AS airplane_seats
FROM airlines
LEFT JOIN flights ON airlines.carrier = flights.carrier
LEFT JOIN airports AS origin_airport ON flights.origin = origin_airport.faa
LEFT JOIN weather ON flights.origin = weather.origin 
                 AND weather.year = flights.year 
                 AND weather.month = flights.month
                 AND weather.day = flights.day
                 AND weather.hour = flights.hour
LEFT JOIN airports AS dest_airport ON flights.dest = origin_airport.faa
LEFT JOIN planes ON flights.tailnum = planes.tailnum
"""


def load_database(spark, jdbc_url, user, password):
    tables = ["airlines", "airports", "flights", "planes", "weather"]

    for table in tables:
        df = read_from_db(spark, jdbc_url, table, user, password)
        df.createOrReplaceTempView(table)

def run_query(spark):
    return spark.sql(query)
