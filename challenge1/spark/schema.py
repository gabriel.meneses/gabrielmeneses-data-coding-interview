schema = {
    "airlines": {
        "carrier": {
            "name": "carrier",
            "type": "STRING"
        },
        "name": {
            "name": "name",
            "type": "STRING"
        },
    },
    "airports": {
        "faa": {
            "name": "faa",
            "type": "STRING"
        },
        "name": {
            "name": "name",
            "type": "STRING"
        },
        "lat": {
            "name": "latitude",
            "type": "DOUBLE"
        },
        "lon": {
            "name": "longitude",
            "type": "DOUBLE"
        },
        "alt": {
            "name": "altitude",
            "type": "DOUBLE"
        },
        "tz": {
            "name": "timezone",
            "type": "INTEGER"
        },
        "dst": {
            "name": "dst",
            "type": "STRING"
        },
        "tzone": {
            "name": "timezone_name",
            "type": "STRING"
        }
    },
    "planes": {
        "tailnum": {
            "name": "tailnum",
            "type": "STRING"
        },
        "year": {
            "name": "year",
            "type": "INTEGER"
        },
        "type": {
            "name": "type",
            "type": "STRING"
        },
        "manufacturer": {
            "name": "manufacturer",
            "type": "STRING"
        },
        "model": {
            "name": "model",
            "type": "STRING"
        },
        "engines": {
            "name": "engines",
            "type": "INTEGER"
        },
        "seats": {
            "name": "seats",
            "type": "INTEGER"
        },
        "speed": {
            "name": "speed",
            "type": "DOUBLE"
        },
        "engine": {
            "name": "engine",
            "type": "STRING"
        }
    },
    "weather": {
        "origin": {
            "name": "origin",
            "type": "STRING"
        },
        "year": {
            "name": "year",
            "type": "INTEGER"
        },
        "month": {
            "name": "month",
            "type": "INTEGER"
        },
        "day": {
            "name": "day",
            "type": "INTEGER"
        },
        "hour": {
            "name": "hour",
            "type": "INTEGER"
        },
        "temp": {
            "name": "temp",
            "type": "DOUBLE"
        },
        "dewp": {
            "name": "dewp",
            "type": "DOUBLE"
        },
        "humid": {
            "name": "humid",
            "type": "DOUBLE"
        },
        "wind_dir": {
            "name": "wind_dir",
            "type": "DOUBLE"
        },
        "wind_speed": {
            "name": "wind_speed",
            "type": "DOUBLE"
        },
        "wind_gust": {
            "name": "wind_gust",
            "type": "DOUBLE"
        },
        "precip": {
            "name": "precip",
            "type": "DOUBLE"
        },
        "pressure": {
            "name": "pressure",
            "type": "DOUBLE"
        },
        "visib": {
            "name": "visib",
            "type": "DOUBLE"
        },
        "time_hour": {
            "name": "time_hour",
            "type": "TIMESTAMP"
        }
    },
    "flights": {
        "carrier": {
            "name": "carrier",
            "type": "STRING"
        },
        "flight": {
            "name": "flight",
            "type": "INTEGER"
        },
        "year": {
            "name": "year",
            "type": "INTEGER"
        },
        "month": {
            "name": "month",
            "type": "INTEGER"
        },
        "day": {
            "name": "day",
            "type": "INTEGER"
        },
        "hour": {
            "name": "hour",
            "type": "INTEGER"
        },
        "minute": {
            "name": "minute",
            "type": "INTEGER"
        },
        "dep_time": {
            "name": "actual_dep_time",
            "type": "INTEGER"
        },
        "sched_dep_time": {
            "name": "sched_dep_time",
            "type": "INTEGER"
        },
        "dep_delay": {
            "name": "dep_delay",
            "type": "INTEGER"
        },
        "arr_time": {
            "name": "actual_arr_time",
            "type": "INTEGER"
        },
        "sched_arr_time": {
            "name": "sched_arr_time",
            "type": "INTEGER"
        },
        "arr_delay": {
            "name": "arr_delay",
            "type": "INTEGER"
        },
        "tailnum": {
            "name": "tailnum",
            "type": "STRING"
        },
        "origin": {
            "name": "origin",
            "type": "STRING"
        },
        "dest": {
            "name": "dest",
            "type": "STRING"
        },
        "air_time": {
            "name": "air_time",
            "type": "DOUBLE"
        },
        "distance": {
            "name": "distance",
            "type": "DOUBLE"
        },
        "time_hour": {
            "name": "time_hour",
            "type": "TIMESTAMP"
        }
    }
}

primary_keys = {
    "airlines": ["carrier"],
    "airports": ["faa"],
    "planes": ["tailnum"],
    "weather": ["origin", "year", "month", "day", "hour"],
    "flights": ["carrier", "flight", "year", "month", "day", "hour", "minute"]
}
